********************************************************************************
* DUPLICATOR-LITE: Install-Log
* STEP-1 START @ 11:20:46
* VERSION: 1.4.5
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
PACKAGE INFO________ CURRENT SERVER                         |ORIGINAL SERVER
PHP VERSION_________: 7.4.29                                |7.4.3
OS__________________: Linux                                 |Linux
CREATED_____________: 2022-04-20 11:12:20
WP VERSION__________: 5.9.3
DUP VERSION_________: 1.4.5
DB__________________: 10.3.34
DB TABLES___________: 13
DB ROWS_____________: 627
DB FILE SIZE________: 5.2MB
********************************************************************************
SERVER INFO
PHP_________________: 7.4.3 | SAPI: apache2handler
PHP MEMORY__________: 4294967296 | SUHOSIN: disabled
SERVER______________: Apache/2.4.41 (Ubuntu)
DOC ROOT____________: "/home/pc2/vhosts/solarcraft.local"
DOC ROOT 755________: true
LOG FILE 644________: true
REQUEST URL_________: "http://solarcraft.local/dup-installer/main.installer.php"
********************************************************************************
USER INPUTS
ARCHIVE ACTION______: "donothing"
ARCHIVE ENGINE______: "shellexec_unzip"
SET DIR PERMS_______: 1
DIR PERMS VALUE_____: 1363
SET FILE PERMS______: 1
FILE PERMS VALUE____: 1204
SAFE MODE___________: "0"
LOGGING_____________: "1"
CONFIG MODE_________: "NEW"
FILE TIME___________: "current"
********************************************************************************


--------------------------------------
ARCHIVE SETUP
--------------------------------------
NAME________________: "/home/pc2/vhosts/solarcraft.local/20220420_horizon_[HASH]_20220420111220_archive.zip"
SIZE________________: 102.07MB

PRE-EXTRACT-CHECKS
- PASS: Apache '.htaccess' not found - no backup needed.
- PASS: Microsoft IIS 'web.config' not found - no backup needed.
- PASS: WordFence '.user.ini' not found - no backup needed.
BEFORE EXTRACION ACTIONS
MAINTENANCE MODE ENABLE


START ZIP FILE EXTRACTION SHELLEXEC >>> 
<<< Shell-Exec Unzip Complete.
--------------------------------------
POST-EXTRACT-CHECKS
--------------------------------------
PERMISSION UPDATES:
    -DIRS:  '755'
    -FILES: '644'
[PHP ERR][WARN] MSG:chmod(): Operation not permitted [CODE:2|FILE:/home/pc2/vhosts/solarcraft.local/dup-installer/lib/snaplib/class.snaplib.u.io.php|LINE:690]
Permissions setting on file '/home/pc2/vhosts/solarcraft.local/installer.php' failed
[PHP ERR][WARN] MSG:touch(): Utime failed: Permission denied [CODE:2|FILE:/home/pc2/vhosts/solarcraft.local/dup-installer/ctrls/classes/class.ctrl.extraction.php|LINE:571]
[PHP ERR][WARN] MSG:touch(): Utime failed: Permission denied [CODE:2|FILE:/home/pc2/vhosts/solarcraft.local/dup-installer/ctrls/classes/class.ctrl.extraction.php|LINE:571]
[PHP ERR][WARN] MSG:touch(): Utime failed: Permission denied [CODE:2|FILE:/home/pc2/vhosts/solarcraft.local/dup-installer/ctrls/classes/class.ctrl.extraction.php|LINE:571]
[PHP ERR][WARN] MSG:chmod(): Operation not permitted [CODE:2|FILE:/home/pc2/vhosts/solarcraft.local/dup-installer/lib/snaplib/class.snaplib.u.io.php|LINE:690]
Permissions setting on file '/home/pc2/vhosts/solarcraft.local/20220420_horizon_[HASH]_20220420111220_archive.zip' failed
[PHP ERR][WARN] MSG:touch(): Utime failed: Permission denied [CODE:2|FILE:/home/pc2/vhosts/solarcraft.local/dup-installer/ctrls/classes/class.ctrl.extraction.php|LINE:571]

STEP-1 COMPLETE @ 11:20:48 - RUNTIME: 2.2815 sec.



********************************************************************************
* DUPLICATOR-LITE INSTALL-LOG
* STEP-2 START @ 11:21:03
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
USER INPUTS
VIEW MODE___________: "basic"
DB ACTION___________: "empty"
DB HOST_____________: "**OBSCURED**"
DB NAME_____________: "**OBSCURED**"
DB PASS_____________: "**OBSCURED**"
DB PORT_____________: "**OBSCURED**"
NON-BREAKING SPACES_: false
MYSQL MODE__________: "DEFAULT"
MYSQL MODE OPTS_____: ""
CHARSET_____________: "utf8"
COLLATE_____________: "utf8_general_ci"
COLLATE FB__________: false
VIEW CREATION_______: true
STORED PROCEDURE____: true
FUNCTION CREATION___: true
********************************************************************************

--------------------------------------
DATABASE-ENVIRONMENT
--------------------------------------
MYSQL VERSION:	This Server: 8.0.28 -- Build Server: 10.3.34
FILE SIZE:	dup-database__[HASH].sql (3.18MB)
TIMEOUT:	5000
MAXPACK:	67108864
SQLMODE:	ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
NEW SQL FILE:	[/home/pc2/vhosts/solarcraft.local/dup-installer/dup-installer-data__[HASH].sql]
COLLATE FB:	Off

NOTICE: This servers version [8.0.28] is less than the build version [10.3.34].  
If you find issues after testing your site please referr to this FAQ item.
https://snapcreek.com/duplicator/docs/faqs-tech/#faq-installer-260-q
--------------------------------------
DATABASE RESULTS
--------------------------------------
DB VIEWS:	enabled
DB PROCEDURES:	enabled
DB FUNCTIONS:	enabled
ERRORS FOUND:	0
DROPPED TABLES:	0
RENAMED TABLES:	0
QUERIES RAN:	126

tFChtNB8a_commentmeta: (0)
tFChtNB8a_comments: (0)
tFChtNB8a_duplicator_packages: (0)
tFChtNB8a_links: (0)
tFChtNB8a_options: (138)
tFChtNB8a_postmeta: (271)
tFChtNB8a_posts: (170)
tFChtNB8a_term_relationships: (16)
tFChtNB8a_term_taxonomy: (3)
tFChtNB8a_termmeta: (0)
tFChtNB8a_terms: (3)
tFChtNB8a_usermeta: (19)
tFChtNB8a_users: (1)
Removed '33' cache/transient rows

INSERT DATA RUNTIME: 4.0971 sec.
STEP-2 COMPLETE @ 11:21:07 - RUNTIME: 4.1165 sec.

====================================
SET SEARCH AND REPLACE LIST
====================================


********************************************************************************
DUPLICATOR PRO INSTALL-LOG
STEP-3 START @ 11:21:26
NOTICE: Do NOT post to public sites or forums
********************************************************************************
CHARSET SERVER:	"utf8"
CHARSET CLIENT:	"utf8"
********************************************************************************
OPTIONS:
blogname______________: "horizon"
postguid______________: false
fullsearch____________: false
path_old______________: "/var/www/vhosts/brave-lalande.185-133-207-10.plesk.page/httpdocs"
path_new______________: "/home/pc2/vhosts/solarcraft.local"
siteurl_______________: "http://solarcraft.local"
url_old_______________: "https://brave-lalande.185-133-207-10.plesk.page"
url_new_______________: "http://solarcraft.local"
maxSerializeStrlen____: 4000000
replaceMail___________: false
dbcharset_____________: "utf8"
dbcollate_____________: "utf8_general_ci"
wp_mail_______________: ""
wp_nickname___________: ""
wp_first_name_________: ""
wp_last_name__________: ""
ssl_admin_____________: false
cache_wp______________: false
cache_path____________: false
exe_safe_mode_________: false
config_mode___________: "NEW"
********************************************************************************


====================================
RUN SEARCH AND REPLACE
====================================

EVALUATE TABLE: "tFChtNB8a_commentmeta"___________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "tFChtNB8a_comments"______________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "tFChtNB8a_duplicator_packages"___________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "tFChtNB8a_links"_________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "tFChtNB8a_options"_______________________________[ROWS:   138][PG:   1][SCAN:text columns]
	SEARCH  1:"/var/www/vhosts/brave-lalande.185-133-207-10.plesk.page/httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  2:"\/var\/www\/vhosts\/brave-lalande.185-133-207-10.plesk.page\/httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  3:"%2Fvar%2Fwww%2Fvhosts%2Fbrave-lalande.185-133-207-10.plesk.page%2Fhttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  4:"\var\www\vhosts\brave-lalande.185-133-207-10.plesk.page\httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  5:"\\var\\www\\vhosts\\brave-lalande.185-133-207-10.plesk.page\\httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  6:"%5Cvar%5Cwww%5Cvhosts%5Cbrave-lalande.185-133-207-10.plesk.page%5Chttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  7:"//brave-lalande.185-133-207-10.plesk.page" =======> "//solarcraft.local"
	SEARCH  8:"\/\/brave-lalande.185-133-207-10.plesk.page" =====> "\/\/solarcraft.local"
	SEARCH  9:"%2F%2Fbrave-lalande.185-133-207-10.plesk.page" ===> "%2F%2Fsolarcraft.local"
	SEARCH 10:"https://solarcraft.local" ========================> "http://solarcraft.local"
	SEARCH 11:"https:\/\/solarcraft.local" ======================> "http:\/\/solarcraft.local"
	SEARCH 12:"https%3A%2F%2Fsolarcraft.local" ==================> "http%3A%2F%2Fsolarcraft.local"

EVALUATE TABLE: "tFChtNB8a_postmeta"______________________________[ROWS:   271][PG:   1][SCAN:text columns]
	SEARCH  1:"/var/www/vhosts/brave-lalande.185-133-207-10.plesk.page/httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  2:"\/var\/www\/vhosts\/brave-lalande.185-133-207-10.plesk.page\/httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  3:"%2Fvar%2Fwww%2Fvhosts%2Fbrave-lalande.185-133-207-10.plesk.page%2Fhttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  4:"\var\www\vhosts\brave-lalande.185-133-207-10.plesk.page\httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  5:"\\var\\www\\vhosts\\brave-lalande.185-133-207-10.plesk.page\\httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  6:"%5Cvar%5Cwww%5Cvhosts%5Cbrave-lalande.185-133-207-10.plesk.page%5Chttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  7:"//brave-lalande.185-133-207-10.plesk.page" =======> "//solarcraft.local"
	SEARCH  8:"\/\/brave-lalande.185-133-207-10.plesk.page" =====> "\/\/solarcraft.local"
	SEARCH  9:"%2F%2Fbrave-lalande.185-133-207-10.plesk.page" ===> "%2F%2Fsolarcraft.local"
	SEARCH 10:"https://solarcraft.local" ========================> "http://solarcraft.local"
	SEARCH 11:"https:\/\/solarcraft.local" ======================> "http:\/\/solarcraft.local"
	SEARCH 12:"https%3A%2F%2Fsolarcraft.local" ==================> "http%3A%2F%2Fsolarcraft.local"

EVALUATE TABLE: "tFChtNB8a_posts"_________________________________[ROWS:   170][PG:   1][SCAN:text columns]
	SEARCH  1:"/var/www/vhosts/brave-lalande.185-133-207-10.plesk.page/httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  2:"\/var\/www\/vhosts\/brave-lalande.185-133-207-10.plesk.page\/httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  3:"%2Fvar%2Fwww%2Fvhosts%2Fbrave-lalande.185-133-207-10.plesk.page%2Fhttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  4:"\var\www\vhosts\brave-lalande.185-133-207-10.plesk.page\httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  5:"\\var\\www\\vhosts\\brave-lalande.185-133-207-10.plesk.page\\httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  6:"%5Cvar%5Cwww%5Cvhosts%5Cbrave-lalande.185-133-207-10.plesk.page%5Chttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  7:"//brave-lalande.185-133-207-10.plesk.page" =======> "//solarcraft.local"
	SEARCH  8:"\/\/brave-lalande.185-133-207-10.plesk.page" =====> "\/\/solarcraft.local"
	SEARCH  9:"%2F%2Fbrave-lalande.185-133-207-10.plesk.page" ===> "%2F%2Fsolarcraft.local"
	SEARCH 10:"https://solarcraft.local" ========================> "http://solarcraft.local"
	SEARCH 11:"https:\/\/solarcraft.local" ======================> "http:\/\/solarcraft.local"
	SEARCH 12:"https%3A%2F%2Fsolarcraft.local" ==================> "http%3A%2F%2Fsolarcraft.local"

EVALUATE TABLE: "tFChtNB8a_term_relationships"____________________[ROWS:    16][PG:   1][SCAN:text columns]
	SEARCH  1:"/var/www/vhosts/brave-lalande.185-133-207-10.plesk.page/httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  2:"\/var\/www\/vhosts\/brave-lalande.185-133-207-10.plesk.page\/httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  3:"%2Fvar%2Fwww%2Fvhosts%2Fbrave-lalande.185-133-207-10.plesk.page%2Fhttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  4:"\var\www\vhosts\brave-lalande.185-133-207-10.plesk.page\httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  5:"\\var\\www\\vhosts\\brave-lalande.185-133-207-10.plesk.page\\httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  6:"%5Cvar%5Cwww%5Cvhosts%5Cbrave-lalande.185-133-207-10.plesk.page%5Chttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  7:"//brave-lalande.185-133-207-10.plesk.page" =======> "//solarcraft.local"
	SEARCH  8:"\/\/brave-lalande.185-133-207-10.plesk.page" =====> "\/\/solarcraft.local"
	SEARCH  9:"%2F%2Fbrave-lalande.185-133-207-10.plesk.page" ===> "%2F%2Fsolarcraft.local"
	SEARCH 10:"https://solarcraft.local" ========================> "http://solarcraft.local"
	SEARCH 11:"https:\/\/solarcraft.local" ======================> "http:\/\/solarcraft.local"
	SEARCH 12:"https%3A%2F%2Fsolarcraft.local" ==================> "http%3A%2F%2Fsolarcraft.local"

EVALUATE TABLE: "tFChtNB8a_term_taxonomy"_________________________[ROWS:     3][PG:   1][SCAN:text columns]
	SEARCH  1:"/var/www/vhosts/brave-lalande.185-133-207-10.plesk.page/httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  2:"\/var\/www\/vhosts\/brave-lalande.185-133-207-10.plesk.page\/httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  3:"%2Fvar%2Fwww%2Fvhosts%2Fbrave-lalande.185-133-207-10.plesk.page%2Fhttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  4:"\var\www\vhosts\brave-lalande.185-133-207-10.plesk.page\httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  5:"\\var\\www\\vhosts\\brave-lalande.185-133-207-10.plesk.page\\httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  6:"%5Cvar%5Cwww%5Cvhosts%5Cbrave-lalande.185-133-207-10.plesk.page%5Chttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  7:"//brave-lalande.185-133-207-10.plesk.page" =======> "//solarcraft.local"
	SEARCH  8:"\/\/brave-lalande.185-133-207-10.plesk.page" =====> "\/\/solarcraft.local"
	SEARCH  9:"%2F%2Fbrave-lalande.185-133-207-10.plesk.page" ===> "%2F%2Fsolarcraft.local"
	SEARCH 10:"https://solarcraft.local" ========================> "http://solarcraft.local"
	SEARCH 11:"https:\/\/solarcraft.local" ======================> "http:\/\/solarcraft.local"
	SEARCH 12:"https%3A%2F%2Fsolarcraft.local" ==================> "http%3A%2F%2Fsolarcraft.local"

EVALUATE TABLE: "tFChtNB8a_termmeta"______________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "tFChtNB8a_terms"_________________________________[ROWS:     3][PG:   1][SCAN:text columns]
	SEARCH  1:"/var/www/vhosts/brave-lalande.185-133-207-10.plesk.page/httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  2:"\/var\/www\/vhosts\/brave-lalande.185-133-207-10.plesk.page\/httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  3:"%2Fvar%2Fwww%2Fvhosts%2Fbrave-lalande.185-133-207-10.plesk.page%2Fhttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  4:"\var\www\vhosts\brave-lalande.185-133-207-10.plesk.page\httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  5:"\\var\\www\\vhosts\\brave-lalande.185-133-207-10.plesk.page\\httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  6:"%5Cvar%5Cwww%5Cvhosts%5Cbrave-lalande.185-133-207-10.plesk.page%5Chttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  7:"//brave-lalande.185-133-207-10.plesk.page" =======> "//solarcraft.local"
	SEARCH  8:"\/\/brave-lalande.185-133-207-10.plesk.page" =====> "\/\/solarcraft.local"
	SEARCH  9:"%2F%2Fbrave-lalande.185-133-207-10.plesk.page" ===> "%2F%2Fsolarcraft.local"
	SEARCH 10:"https://solarcraft.local" ========================> "http://solarcraft.local"
	SEARCH 11:"https:\/\/solarcraft.local" ======================> "http:\/\/solarcraft.local"
	SEARCH 12:"https%3A%2F%2Fsolarcraft.local" ==================> "http%3A%2F%2Fsolarcraft.local"

EVALUATE TABLE: "tFChtNB8a_usermeta"______________________________[ROWS:    19][PG:   1][SCAN:text columns]
	SEARCH  1:"/var/www/vhosts/brave-lalande.185-133-207-10.plesk.page/httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  2:"\/var\/www\/vhosts\/brave-lalande.185-133-207-10.plesk.page\/httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  3:"%2Fvar%2Fwww%2Fvhosts%2Fbrave-lalande.185-133-207-10.plesk.page%2Fhttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  4:"\var\www\vhosts\brave-lalande.185-133-207-10.plesk.page\httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  5:"\\var\\www\\vhosts\\brave-lalande.185-133-207-10.plesk.page\\httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  6:"%5Cvar%5Cwww%5Cvhosts%5Cbrave-lalande.185-133-207-10.plesk.page%5Chttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  7:"//brave-lalande.185-133-207-10.plesk.page" =======> "//solarcraft.local"
	SEARCH  8:"\/\/brave-lalande.185-133-207-10.plesk.page" =====> "\/\/solarcraft.local"
	SEARCH  9:"%2F%2Fbrave-lalande.185-133-207-10.plesk.page" ===> "%2F%2Fsolarcraft.local"
	SEARCH 10:"https://solarcraft.local" ========================> "http://solarcraft.local"
	SEARCH 11:"https:\/\/solarcraft.local" ======================> "http:\/\/solarcraft.local"
	SEARCH 12:"https%3A%2F%2Fsolarcraft.local" ==================> "http%3A%2F%2Fsolarcraft.local"

EVALUATE TABLE: "tFChtNB8a_users"_________________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"/var/www/vhosts/brave-lalande.185-133-207-10.plesk.page/httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  2:"\/var\/www\/vhosts\/brave-lalande.185-133-207-10.plesk.page\/httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  3:"%2Fvar%2Fwww%2Fvhosts%2Fbrave-lalande.185-133-207-10.plesk.page%2Fhttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  4:"\var\www\vhosts\brave-lalande.185-133-207-10.plesk.page\httpdocs" => "/home/pc2/vhosts/solarcraft.local"
	SEARCH  5:"\\var\\www\\vhosts\\brave-lalande.185-133-207-10.plesk.page\\httpdocs" => "\/home\/pc2\/vhosts\/solarcraft.local"
	SEARCH  6:"%5Cvar%5Cwww%5Cvhosts%5Cbrave-lalande.185-133-207-10.plesk.page%5Chttpdocs" => "%2Fhome%2Fpc2%2Fvhosts%2Fsolarcraft.local"
	SEARCH  7:"//brave-lalande.185-133-207-10.plesk.page" =======> "//solarcraft.local"
	SEARCH  8:"\/\/brave-lalande.185-133-207-10.plesk.page" =====> "\/\/solarcraft.local"
	SEARCH  9:"%2F%2Fbrave-lalande.185-133-207-10.plesk.page" ===> "%2F%2Fsolarcraft.local"
	SEARCH 10:"https://solarcraft.local" ========================> "http://solarcraft.local"
	SEARCH 11:"https:\/\/solarcraft.local" ======================> "http:\/\/solarcraft.local"
	SEARCH 12:"https%3A%2F%2Fsolarcraft.local" ==================> "http%3A%2F%2Fsolarcraft.local"
--------------------------------------
SCANNED:	Tables:13 	|	 Rows:621 	|	 Cells:5710 
UPDATED:	Tables:3 	|	 Rows:174 	|	 Cells:178 
ERRORS:		0 
RUNTIME:	0.286300 sec

====================================
REMOVE LICENSE KEY
====================================

====================================
CREATE NEW ADMIN USER
====================================

====================================
CONFIGURATION FILE UPDATES
====================================
	UPDATE DB_NAME "** OBSCURED **"
	UPDATE DB_USER "** OBSCURED **"
	UPDATE DB_PASSWORD "** OBSCURED **"
	UPDATE DB_HOST ""localhost""
	REMOVE WPCACHEHOME
	
*** UPDATED WP CONFIG FILE ***

====================================
HTACCESS UPDATE MODE: "NEW"
====================================
- PASS: Successfully created a new .htaccess file.
- PASS: Existing Apache '.htaccess__[HASH]' was removed

====================================
GENERAL UPDATES & CLEANUP
====================================

====================================
DEACTIVATE PLUGINS CHECK
====================================
Auto Deactivated plugins list here: Array
(
    [0] => really-simple-ssl/rlrsssl-really-simple-ssl.php
    [1] => simple-google-recaptcha/simple-google-recaptcha.php
    [2] => js_composer/js_composer.php
)

MAINTENANCE MODE DISABLE

====================================
NOTICES TEST
====================================
No General Notices Found


====================================
CLEANUP TMP FILES
====================================

====================================
FINAL REPORT NOTICES
====================================

STEP-3 COMPLETE @ 11:21:27 - RUNTIME: 0.3288 sec. 


====================================
FINAL REPORT NOTICES LIST
====================================
-----------------------
[INFO] No general notices
	SECTIONS: general

-----------------------
[INFO] No errors in database
	SECTIONS: database

-----------------------
[INFO] No search and replace data errors
	SECTIONS: search_replace

-----------------------
[INFO] No files extraction errors
	SECTIONS: files

====================================
