<?php // $file = /var/www/vhosts/brave-lalande.185-133-207-10.plesk.page/httpdocs/wp-content/themes/yootheme/vendor/yootheme/builder-wordpress-source/config/customizer.json

return [
  'sources' => [
    'postTypeOrderOptions' => [[
        'text' => 'Date', 
        'value' => 'date'
      ], [
        'text' => 'Modified', 
        'value' => 'modified'
      ], [
        'text' => 'Alphabetical', 
        'value' => 'title'
      ], [
        'text' => 'Author', 
        'value' => 'author'
      ], [
        'text' => 'Post Order', 
        'value' => 'menu_order'
      ], [
        'text' => 'Comment Count', 
        'value' => 'comment_count'
      ], [
        'text' => 'Random', 
        'value' => 'rand'
      ]]
  ]
];
