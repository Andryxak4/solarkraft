<?php // $file = /var/www/vhosts/brave-lalande.185-133-207-10.plesk.page/httpdocs/wp-content/themes/yootheme/vendor/yootheme/theme-cookie/config/theme.json

return [
  'defaults' => [
    'cookie' => [
      'type' => 'bar', 
      'bar_position' => 'bottom', 
      'bar_style' => 'muted', 
      'notification_position' => 'bottom-center', 
      'message' => 'By using this website, you agree to the use of cookies as described in our Privacy Policy.', 
      'button_consent_style' => 'icon', 
      'button_consent_text' => 'Ok', 
      'button_reject_style' => 'default', 
      'button_reject_text' => 'No, Thanks'
    ]
  ]
];
